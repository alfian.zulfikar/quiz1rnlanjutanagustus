import React, {useContext} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {RootContext} from './index';

export default function TodoList() {
  const state = useContext(RootContext);

  return (
    <View style={styles.container}>
      {/* list */}
      <View style={styles.list}>
        <FlatList
          data={state.name}
          renderItem={({item}) => (
            <View style={styles.listItem}>
              <View>
                <Text>{item.name.name}</Text>
                <Text>{item.name.position}</Text>
              </View>
              <TouchableOpacity onPress={() => state.pressHandler(item.key)}>
                <Icon name="delete" size={24} color="black" />
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    marginHorizontal: 20,
  },
});
