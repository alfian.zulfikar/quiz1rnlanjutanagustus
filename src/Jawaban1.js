import React, {useState} from 'react';
import {Text, View} from 'react-native';

export default function FComponent() {
  const [name, setName] = useState('Jhon Dae');

  useEffect(() => {
    setTimeout(() => {
      this.setState({
        name: 'Asep',
      });
    }, 3000);
    return () => {
      setName('Asep');
    };
  }, [name]);

  return (
    <View>
      <Text>{this.state.name}</Text>
    </View>
  );
}
